# Mesa de Entradas Virtual

## Requerimientos

- PHP >= 5.4
- composer
- composer assert plugin

El **composer assert plugin** debe instalarse una vez que se tiene instalado **composer** con la siguiente instrucción:

```bash
composer global require "fxp/composer-asset-plugin:^1.4.1"
```

## Instalación

Después de clonar el proyecto, ejecutar sobre el directorio raíz:

```bash
composer install
```

## Configuración

Existe un archivo `.env.sampple` en la raíz del proyecto, el cual es el esqueleto para la configuración de la aplicación. En base a este, debe ser creado un archivo `.env` el cual será ignorado por Git. Sobre este archivo se deben establecer las conexiones de datos y el entorno de ejecución de la aplicación.

## Ejecución

La ejecución puede hacerse mediante la consola de Yii con el comando

```bash
php yii serve --port=8888
```

con lo que se generar un servidor web _ad-hoc_ sobre este puerto.

Para ejecutarlo sobre un servidor web ya configurado, se debe crear un **host virtual** que apunte al directorio **web**. En este directorio se encuentra el punto de entrada **index.php** para el sitio.


<?php

namespace app\models;

/**
 * @property integer $codigoe Código del empleado
 * @property string $apellido Apellido del empleado
 * @property string $abreo
 * @property integer $cgodestino
 * @property string $clavencrypt
 * @property string $programa
 * @property integer $nrodoc
 * @property string $abrevia
 * @property integer $MagoFunc
 * @property string $sistema
 * @property integer $codorganismo
 * @property integer $codlocalidad
 */

class Usuario extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    /**
     * @var integer $codigo
     */
    public $codigoe;

    /**
     * @var string $apellido
     */
    public $apellido;

    /**
     * @var string $abrevo
     */
    public $abreo;

    /**
     * @var integer $cgodestino
     */
    public $cgodestino;

    /**
     * @var string $claveencrypt
     */
    public $claveencrypt;

    /**
     * @var string $programa
     */
   public $programa;
  
    /**
     * @var integer $nrodoc
     */
   public $nrodoc;
  
    /**
     * @var string $abrevia
     */
   public $abrevia;
  
    /**
     * @var integer $Magofunc
     */
   public $Magofunc;
  
    /**
     * @var integer $coorganismo
     */
   public $sistema;
 
    /**
     * @var integer $coorganismo
     */
   public $codorganismo;

    /**
     * @var integer $codlocalidad
     */
    public $codlocalidad;

    public static function getDb()
    {
        return \Yii::$app->loginDb;
    }

    public static primaryKey()
    {
        return ['codigoe'];
    }

    /**
     * Recupera un usuario a partir de su código de empleado
     * @param integer $codigoe
     * @return Usuario
     */
    public static function findIdentity($codigoe)
    {
        static::findOne($codigoe);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        static::find()->where(['abrevia' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->codigoe;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        static::find()->where(['codigoe' => $this->codigoe])
            ->andWhere("PWDCOMPARE(:pwd, clavencrypt) = 1", [':pwd' => $password])
            ->count();
    }
}

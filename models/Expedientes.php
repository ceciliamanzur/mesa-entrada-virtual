<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expedientes".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property string|null $fecha_hora_insert
 * @property string|null $usuario_insert
 * @property string|null $fecha_hora_update
 * @property string|null $usuario_update
 * @property int|null $organismo_actual
 * @property string|null $observacion
 */
class Expedientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expedientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion', 'observacion'], 'string'],
            [['fecha_hora_insert', 'fecha_hora_update'], 'safe'],
            [['organismo_actual'], 'integer'],
            [['usuario_insert', 'usuario_update'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'fecha_hora_insert' => 'Fecha Ingreso',
            'usuario_insert' => 'Presentante',
            'fecha_hora_update' => 'Fecha Hora Update',
            'usuario_update' => 'Usuario Update',
            'organismo_actual' => 'Organismo Actual',
            'observacion' => 'Observacion',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movimientos".
 *
 * @property int $id
 * @property int|null $estado_id
 * @property string|null $fecha_hora_insert
 * @property string|null $usuario_insert
 * @property string|null $fecha_hora_update
 * @property string|null $usuario_update
 * @property string|null $descripcion
 * @property int|null $activo
 * @property int|null $expedientes_id
 * @property int|null $organismo_id
 */
class Movimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado_id', 'activo', 'expedientes_id', 'organismo_id'], 'integer'],
            [['fecha_hora_insert', 'fecha_hora_update'], 'safe'],
            [['descripcion'], 'string'],
            [['usuario_insert', 'usuario_update'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estado_id' => 'Estado ID',
            'fecha_hora_insert' => 'Fecha Hora Insert',
            'usuario_insert' => 'Usuario Insert',
            'fecha_hora_update' => 'Fecha Hora Update',
            'usuario_update' => 'Usuario Update',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
            'expedientes_id' => 'Expedientes ID',
            'organismo_id' => 'Organismo ID',
        ];
    }
}

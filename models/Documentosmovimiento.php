<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documentosmovimiento".
 *
 * @property int $id
 * @property string|null $archivo
 * @property int|null $activo
 */
class Documentosmovimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documentosmovimiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo'], 'integer'],
            [['archivo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'archivo' => 'Archivo',
            'activo' => 'Activo',
        ];
    }
}

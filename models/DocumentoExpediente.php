<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documento_expediente".
 *
 * @property int $id
 * @property string|null $documento
 * @property int|null $expedientes_id
 */
class DocumentoExpediente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documento_expediente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['expedientes_id'], 'integer'],
            [['documento'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'documento' => 'Documento',
            'expedientes_id' => 'Expedientes ID',
        ];
    }
}

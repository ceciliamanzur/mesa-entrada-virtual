<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=mesa_virtual',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    //'class' => 'yii\db\Connection',
    //'dsn' => getenv('DB_DSN'),
    //'username' => getenv('DB_USER'),
    //'password' => getenv('DB_PASSWD'),
    //'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];

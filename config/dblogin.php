<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv('DB_LOGIN_DSN'),
    'username' => getenv('DB_LOGIN_USER'),
    'password' => getenv('DB_LOGIN_PASSWD'),
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];

CREATE TABLE `Expedientes` (
`id` int NULL AUTO_INCREMENT,
`titulo` longtext NULL,
`descripcion` longtext NULL,
`fecha_hora_insert` datetime NULL,
`usuario_insert` varchar(255) NULL,
`fecha_hora_update` datetime NULL,
`usuario_update` varchar(255) NULL,
`organismo_actual` int NULL,
`observacion` text NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `Documento_Expediente` (
`id` int NULL AUTO_INCREMENT,
`documento` varchar(255) NULL,
`expedientes_id` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `Usuarios` (
`id` int NULL AUTO_INCREMENT,
`usuario` varchar(255) NULL,
`password` varchar(255) NULL,
`apellido` varchar(150) NULL,
`nombre` varchar(150) NULL,
`rol` varchar(255) NULL,
`activo` tinyint NULL DEFAULT 0,
PRIMARY KEY (`id`) 
);

CREATE TABLE `Movimientos` (
`id` int NULL AUTO_INCREMENT,
`estado_id` int NULL,
`fecha_hora_insert` datetime NULL,
`usuario_insert` varchar(255) NULL,
`fecha_hora_update` datetime NULL,
`usuario_update` varchar(255) NULL,
`descripcion` longtext NULL,
`activo` tinyint NULL DEFAULT 0,
`expedientes_id` int NULL,
`organismo_id` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `Estados` (
`id` int NULL AUTO_INCREMENT,
`descripcion` varchar(255) NULL,
`activo` tinyint NULL DEFAULT 0,
PRIMARY KEY (`id`) 
);

CREATE TABLE `DocumentosMovimiento` (
`id` int NULL AUTO_INCREMENT,
`archivo` varchar(255) NULL,
`activo` tinyint NULL DEFAULT 0,
PRIMARY KEY (`id`) 
);


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Expedientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="expedientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fecha_hora_insert')->textInput() ?>

    <?= $form->field($model, 'usuario_insert')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_hora_update')->textInput() ?>

    <?= $form->field($model, 'usuario_update')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organismo_actual')->textInput() ?>

    <?= $form->field($model, 'observacion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

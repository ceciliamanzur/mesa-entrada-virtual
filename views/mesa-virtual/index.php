<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mesa Virtual';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expedientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo:ntext',
            'descripcion:ntext',
            'fecha_hora_insert',
            'usuario_insert',
            //'fecha_hora_update',
            //'usuario_update',
            //'organismo_actual',
            //'observacion:ntext',

            //['class' => 'yii\grid\ActionColumn'],
            
            ['class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['style' => 'max-width: 100px'],
                        'template' => '{view}  {asignar_organismo}'],]
        
    ]); ?>


</div>
